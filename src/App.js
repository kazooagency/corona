import React, { useState, useEffect } from "react";
import "./App.css";
import html2canvas from "html2canvas";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";

import MomentUtils from "@date-io/moment";

import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";

function App() {
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (loading) {
      window.scroll(0, 0);
      html2canvas(document.getElementById("text"), {
        height: document.getElementById("text").scrollHeight + 50
      }).then(canvas => {
        setImgData(canvas.toDataURL("image/png"));
      });
    }
  }, [loading]);

  const [imgData, setImgData] = useState(null);
  const [motifValue, setMotifValue] = useState("");
  const [nomPrenomValue, setnomPrenomValue] = useState();
  const [address, setAddress] = useState();
  const [city, setCity] = useState();
  const [birth, setBirth] = useState(null);
  const [today, setToday] = useState(null);

  const handleMotifChange = e => {
    setMotifValue(e.target.value);
  };
  const handleNomPrenomChange = e => {
    setnomPrenomValue(e.target.value);
  };

  const handleOnClick = () => {
    setLoading(true);
  };

  const handleAddressChange = e => {
    setAddress(e.target.value);
  };

  const handleCityChange = e => {
    setCity(e.target.value);
  };

  const getMotifText = motifValue => {
    switch (motifValue) {
      case "travail":
        return "déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle, lorsqu’ils sont indispensables à l’exercice d’activités ne pouvant être organisées sous forme de télétravail (sur justificatif permanent) ou déplacements professionnels ne pouvant être différés ;";
      case "achats":
        return "déplacements pour effectuer des achats de première nécessité dans des établissements autorisés (liste sur gouvernement.fr) ;";
      case "sante":
        return "déplacements pour motif de santé ;";
      case "famille":
        return "déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou la garde d’enfants ;";
      case "sport":
        return "déplacements brefs, à proximité du domicile, liés à l’activité physique individuelle des personnes, à l’exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie.";
    }
  };

  const nomInput = (
    <TextField
      id="standard-basic"
      label="Nom Prénom"
      placeholder="Nom Prénom"
      value={nomPrenomValue}
      onChange={handleNomPrenomChange}
    />
  );

  const adressInput = (
    <TextField
      label={"Demeurant au"}
      aria-label="empty textarea"
      placeholder=""
      value={address}
      onChange={handleAddressChange}
    />
  );

  const cityInput = (
    <TextField
      label={"Fait à"}
      aria-label="empty textarea"
      placeholder="Ville"
      value={city}
      onChange={handleCityChange}
    />
  );

  const motifInput = (
    <FormControl component="fieldset">
      <FormLabel component="legend">Motif</FormLabel>
      <RadioGroup
        aria-label="gender"
        name="gender1"
        value={motifValue}
        onChange={handleMotifChange}
      >
        <FormControlLabel
          value="travail"
          control={<Radio />}
          label={getMotifText("travail")}
        />
        <FormControlLabel
          value="achats"
          control={<Radio />}
          label={getMotifText("achats")}
        />
        <FormControlLabel
          value="sante"
          control={<Radio />}
          label={getMotifText("sante")}
        />
        <FormControlLabel
          value="famille"
          control={<Radio />}
          label={getMotifText("famille")}
        />
        <FormControlLabel
          value="sport"
          control={<Radio />}
          label={getMotifText("sport")}
        />
      </RadioGroup>
    </FormControl>
  );

  const birthInput = (
    <DatePicker
      disableFuture
      openTo="year"
      format="DD/MM/YYYY"
      label="Né le"
      views={["year", "month", "date"]}
      value={birth}
      onChange={setBirth}
    />
  );

  const todayInput = (
    <DatePicker
      format="DD/MM/YYYY"
      label="Le"
      value={today}
      onChange={setToday}
    />
  );

  return (
    <div className="App">
      {!imgData && loading && (
        <div id="text">
          <h1>ATTESTATION DE DÉPLACEMENT DÉROGATOIRE</h1>
          <p>
            En application de l’article 1 er du décret du 16 mars 2020 portant
            réglementation des déplacements dans le cadre de la lutte contre la
            propagation du virus Covid-19 :
          </p>
          <p>Je soussigné(e)</p>
          <p>{`Mme / M. ${nomPrenomValue}`}</p>
          <p>{`Né(e) le : ${birth ? birth.format("DD/MM/YYYY") : null}`}</p>
          <p>{`Demeurant : ${address}`}</p>
          <p>
            certifie que mon déplacement est lié au motif suivant autorisé par
            l’article 1 er du décret du 16 mars 2020 portant réglementation des
            déplacements dans le cadre de la lutte contre la propagation du
            virus Covid-19 :
          </p>
          <p>{getMotifText(motifValue)}</p>
          <p>
            {`Fait à ${city}, le ${today ? today.format("DD/MM/YYYY") : null}`}
          </p>
          <p>(signature)</p>
          <p id="signature">{nomPrenomValue}</p>
        </div>
      )}
      {!loading ? (
        <div>
          <MuiPickersUtilsProvider utils={MomentUtils}>
            {nomInput}
            {birthInput}
            {adressInput}
            {motifInput}
            {cityInput}
            {todayInput}
          </MuiPickersUtilsProvider>
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleOnClick()}
          >
            Signer et générer l'attestation
          </Button>
          <Typography>
            <Link href="https://www.linkedin.com/in/kameronargis/">
              Kaméron
            </Link>
            <br></br>
            <Link href="https://www.linkedin.com/in/antoninribeaud/">
              Antonin
            </Link>
            <br></br>
            <Link href="https://www.linkedin.com/in/karimmakhloufi/">
              Karim
            </Link>
          </Typography>
        </div>
      ) : null}

      {imgData ? <img width="100%" alt={"derogation"} src={imgData} /> : null}
    </div>
  );
}

export default App;
